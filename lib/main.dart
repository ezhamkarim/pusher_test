import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:test_runcloud/pusherpage.dart';
import 'package:test_runcloud/socketiopage.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
void main() {
   HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _current = 'pusher';

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        backgroundColor: Colors.white,
        middle: CupertinoSegmentedControl(
          children: {
            'pusher': Text(
              'pusher',
              style: TextStyle(fontSize: 14),
            ),
            'socketio': Text(
              '  socket.io  ',
              style: TextStyle(fontSize: 14),
            ),
          },
          onValueChanged: (value) {
            setState(() {
              _current = value;
            });
          },
          groupValue: _current,
        ),
      ),
      child: _current == 'pusher' ? PusherPage() : SocketioPage(),
    );
  }
}






